package com.learn.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.learn.rest.dao.UserDAO;
import com.learn.rest.entities.User;

@RequestMapping(value = "/user")
@Controller
public class UserController {

	@Autowired
	private UserDAO userDao;

	/**
	 * Get All users
	 * 
	 */
	@GetMapping(value = "")
	public @ResponseBody List<User> getAllUsers() {
		return userDao.findAll();
	}

	/**
	 * Get user by name
	 */
	@GetMapping(value = "/{name}")
	public @ResponseBody User getUserByName(@PathVariable String name) {
		return userDao.findByName(name);
	}
	
	/**
	 * Add user
	 */
	@PostMapping(value="/add")
	public ResponseEntity<String> addUser(@RequestBody User user) {
		userDao.save(user);
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}
}
