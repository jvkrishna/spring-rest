package com.learn.rest.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.learn.rest.entities.User;

public interface UserDAO extends MongoRepository<User, String> {

	User findByName(String name);
	User findByEmail(String email);
	User findByNameAndEmail(String name,String email);
}
